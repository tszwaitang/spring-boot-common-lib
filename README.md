# spring-common-lib-gradle

## build steps
``./gradlew clean build publishToMavenLocal``

## Note
If you want expose libraries for other project, please use 
```api```
instead of ```implementation```

## TODO
1. get tag with gradle task
2. change dynamic query builder to dynamic type instead of fix
