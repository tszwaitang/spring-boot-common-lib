package com.projectx.common.library.model.dto

import org.springframework.data.jpa.domain.Specification
import java.time.LocalDateTime
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class GenericSpecification<T> (private var criteria: SearchCriteria): Specification<T> {
    override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate {
        val value = criteria.value
        return when (criteria.getOperation()) {
            SearchOperation.GREATER_THAN -> {
                if (value is LocalDateTime) {
                    builder.greaterThan(root.get(criteria.key), value) //TODO: change to generic
                } else {
                    builder.greaterThan(root.get(criteria.key), value.toString())
                }
            }
            SearchOperation.GREATER_THAN_EQUAL -> {
                value.javaClass.kotlin
                if (value is LocalDateTime) {
                    builder.greaterThanOrEqualTo(root.get(criteria.key), value)
                } else {
                    builder.greaterThanOrEqualTo(root.get(criteria.key), value.toString())
                }
            }
            SearchOperation.EQUAL -> {
                builder.equal(root.get<String>(criteria.key), value)
            }
            SearchOperation.LESS_THAN -> {
                if (value is LocalDateTime) {
                    builder.lessThan(root.get(criteria.key), value)
                } else {
                    builder.lessThan(root.get(criteria.key), value.toString())
                }
            }
            SearchOperation.LESS_THAN_EQUAL -> {
                if (value is LocalDateTime) {
                    builder.lessThanOrEqualTo(root.get(criteria.key), value)
                } else {
                    builder.lessThanOrEqualTo(root.get(criteria.key), value.toString())
                }
            }
            SearchOperation.LIKE -> {
                builder.like(root.get(criteria.key), "%$value%")
            }
            SearchOperation.NOT_EQUAL -> {
                builder.notEqual(root.get<String>(criteria.key), value)
            }
        }
    }
}