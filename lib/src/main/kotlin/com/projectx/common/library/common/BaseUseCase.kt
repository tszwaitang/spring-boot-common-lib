package com.projectx.common.library.common

import com.projectx.common.library.model.dto.ResponseDto

interface BaseUseCase<Input, T> {
    fun execute(input: Input): ResponseDto<T>
}