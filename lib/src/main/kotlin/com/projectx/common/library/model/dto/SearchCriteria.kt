package com.projectx.common.library.model.dto

class SearchCriteria(var key: String, var value: Any, operation: SearchOperation) {
    private var operation: SearchOperation
    fun getOperation(): SearchOperation {
        return operation
    }

    fun setOperation(operation: SearchOperation) {
        this.operation = operation
    }

    init {
        this.operation = operation
    }
}

enum class SearchOperation {
    GREATER_THAN, LESS_THAN, GREATER_THAN_EQUAL, LESS_THAN_EQUAL, NOT_EQUAL, EQUAL, LIKE
}