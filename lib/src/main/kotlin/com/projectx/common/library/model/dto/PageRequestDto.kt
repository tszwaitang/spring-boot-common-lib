package com.projectx.common.library.model.dto

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

open class PageRequestDto(
    var pageItem: Int?,
    var page: Int?,
    var sortMode: String?,
    var sortField: String?
)

//val logger = LoggerFactory.getLogger(PageRequestDto::class.java)
fun PageRequestDto.buildPagingAndSorting(): PageRequest {
    //calculate first item
    var itemNumber = 20
    this.pageItem?.let { if (it > 0) itemNumber = it}
    var startPage = 0
    this.page?.let { if (it > 0) startPage = it}
//    logger.info("itemNumber: $itemNumber startItem: $startPage")
    //build sorting
    var sort: Sort? = null
    if (!sortField.isNullOrEmpty()) {
        sort = Sort.by(sortField)
    }

    if (!sortMode.isNullOrEmpty() && "DESC" == sortMode) {
        sort?.let {
            sort = it.descending()
        }
    }

    return if (sort != null) {
        PageRequest.of(startPage, itemNumber, sort!!)
    } else {
        PageRequest.of(startPage, itemNumber)
    }
}