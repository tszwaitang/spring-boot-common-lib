package com.projectx.common.library.errorhandling

class DataNotFoundException(var reason: String? = null): Exception()

class UserNotFoundException(var reason: String?): Exception()