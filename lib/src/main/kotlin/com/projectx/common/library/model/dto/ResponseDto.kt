package com.projectx.common.library.model.dto

data class ResponseDto<T>(var data: T? = null)

data class PaginationDto<T>(var currentPage: Int, var totalPage: Int, var pageItem: Int, var items: T? = null)