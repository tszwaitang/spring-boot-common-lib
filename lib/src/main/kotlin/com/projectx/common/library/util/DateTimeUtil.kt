package com.projectx.common.library.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DateTimeUtil {

    companion object{
        fun transformDate(dateString: String, format: String): LocalDateTime {
            val dateTimeFormatter = DateTimeFormatter.ofPattern(format)
            return LocalDateTime.parse(dateString, dateTimeFormatter)
        }
    }
}